const fs = require('fs');



function convertingData(){
    return new Promise((resolve, reject) =>{
        fs.readFile('../lipsum.txt', 'utf8', function(error, data) {
            if (error) {
                reject(error)
            } else{
                resolve(data)
            }
        })
    })
}

function appendFilename(filename){
    fs.appendFile("./filenames.txt", filename, (error) =>{
        if (error){
            console.log(error);
        }
    })
}

function convertingToUpper(data){
    return new Promise((resolve, reject)=>{
        fs.writeFile('./uppercase.txt', data.toUpperCase(), (error) => {
        if (error) {
            reject(error)
        } else{
            resolve("upper created")
            appendFilename("uppercase.txt" + "\n")
        }
    })
  })
    
}

function convertingToLower(){
    return new Promise((resolve, reject) =>{
        const data = fs.readFileSync("./uppercase.txt", "utf-8");
        fs.writeFile('./lowercase.txt', data.toLowerCase().split(".").join("\n"), function(error){
            if (error) {
                reject(error)
            } else{
                resolve("lower created")
                appendFilename("lowercase.txt" + "\n")
            }
        })
    }) 
}

function sortingTheLines(){
    return new Promise((resolve, reject) =>{
        const data = fs.readFileSync("./lowercase.txt", "utf8")
        fs.writeFile('./sorted.txt', data.split("\n").sort((firstWord, secondWord) => {
            if (firstWord > secondWord){
                return 1
            } else if (firstWord < secondWord){
                return -1
            }
        }).join("\n"), (error)=> {
            if (error) {
                reject(error)
            } else{
                appendFilename("sorted.txt" +"\n")
                resolve("sorting done")
            }
      })     
   })
} 

 
function deletingFiles(){
    return new Promise((resolve, reject) =>{
        const data = fs.readFileSync("./filenames.txt", 'utf8').split("\n")
        console.log
        for (let index=0;index<data.length;index++){
            if (data[index] !== ''){
                fs.unlink(`${data[index]}`, function(error){
                    if (error){
                        reject(error)
                    }
                    else{
                        resolve(`${data[index]} deleted`)
                        console.log(`${data[index]} deleted`)
                    }
                })
            }
        }
    })    
} 


module.exports = {fs,convertingData, convertingToUpper, convertingToLower, sortingTheLines, deletingFiles}


